# Teste surpresa - TDD

## 1 - Compreender o objetivo deste model

A função do model Dealer no sistema é definir todos os dados, relações e regras de negócio que a concessionária terá dentro do Autocommerce.

## 2 - Compreender as relações deste model

### O objetivo do model

O model representa a entidade "cliente concessionária" dentro do sistema. É também responsável por modelar todos os dados e comportamentos do Admin, constituindo praticamente toda a sua base. É Praticamente o coração dos models.

### Relações com outros models

Um Dealer pode ter, associadas a ele, várias instâncias dos seguintes models:

* `Stores`: Dealer tem várias stores, que são lojas;
* `DealerAboutItem`: Campos de texto que podem ser incluídos na página Sobre dentro da loja;
* `DealerCnpj`: Um Dealer pode ter vários CNPJs;
* `Billing`: Faturas de pagamento;
* `Manager`: Usuários com o nível mais alto de permissão dentro do sistema;

Por outro lado, cada Dealer pode ter uma única instância dos seguintes models associada a ele:

* `DesignSetting`: Configurações da paleta de cores; metas para SEO; estilos e scripts;
* `FinancingSetting`: Dados finenceiros da concessionária;
* `IntegrationSetting`: Configurações de integrações com o Bomdapeste, DS Auto e ADset;
* `TermSetting`: Dados legais sobre o contrato de compra;
* `LegalInformationSetting`: Informações legais da loja centradas em um único contato/CNPJ, já que um Dealer pode ter vários deles;
* `DeliverySetting`: Configuração da forma de entrega de veículo (integração com EVO);
* `BankBillets::Account`: Dados associados a transações com boletos;
* `BankBillets::Webhook`: _webhook_ responsável pelos dados de notificações de transações via boleto;
* `Iugu::Webhook`: _webhook_ responsável pelos dados da comunicação com a API da Iugu.

Por fim, um Dealer pode ou não pertencer a um plano de subscrição (model SubscriptionPlan). Essa configuração já possui um padrão definido, mas em breve haverão mais opções.

## 3 - Compreender quais as regras de validação deste models

Especificados no próprio model, apenas há a validação se os campos a seguir não estão vazios, o que impacta diretamente nas actions create e update visto que o objeto não vai poder ser salvo caso haja algum valor faltante:

```ruby
field :name
field :subdomain
field :domain
field :corporate_name
field :cnpj
field :billing_closing_day, default: BILLING_CLOSING_DAY
field :subscription_plan
field :zip_code
field :street
field :number
field :state
field :city
field :neighborhood
field :email
```

> **Observação**: Mesmo com a validação no model do campo `zip_code` ainda foi possível criar uma compania com o campo vazio!

O campo `:billing_closing_day` já possui um valor especificado por padrão, então o não preenchimento desse campo pelo usuário não vai impedir o objeto de salvo/atualizado.

Para garantir o estado consistente do objeto em relação há algumas associações, os seguintes callbacks são executados após a criação do objeto. Basicamente são criados novos registros no banco de dados que devem estar associados ao Dealer em questão.  

## 4 - Compreender quais regras de negócio/comportamento desse model estão sendo cobertos por testes

a) Os testes feitos com o model Brand. Já que Brand e Dealer não possuem um relacionamento feito por associação direta e sim por uma relação indireta a partir do model Product, essa lógica no método brand dentro de Dealer deve ser capaz de retornar marcas que estão cadastradas nos produtos do estoque.

b) O conjunto de testes 'standard settings' e 'relationships' estão desatualizados em relação ao model.

c) Deveriam ser implementados, e não foram:

* Validar se todos os campos obrigatórios impedem de fato o Dealer de ser criado caso estejam faltando;

* Validar todas as associações (relacionamentos) que não estão configuradas com `optional: true`;

* Fazer testes que forjam campos errados para testar as validações, como número mínimo de caracteres para para CNPJ.

## 5 - Testes implementados não satisfatórios que poderiam ser fatorados e assim se tornarem satisfatórios

**Standard settings**: <br>
Os testes de standard settings(Configurações padrão) estão desatualizados/incompletos. Na model Dealer temos definidos 4 relacionamentos que tem um novo registro criado logo após salvar um novo Dealer, atráves do `after_create`. Porém nos testes é verificada a existência de apenas 3 desses registros.

Deve ser criado um teste para verificar também a existência do registro de `FinancingSetting` relacionado ao Dealer.

**Relationships**: <br>
O model Dealer é a representação da própria concessionária dentro do sistema, esta model se relaciona com diversas outras, e os testes dessa seção serverm para garantir que todos os relacionamentos estão funcionando corretamente. Entretando, assim como os testes de standard settings, os testes de relacionamentos estão incompletos, apenas alguns relacionamentos estão sendo testados. Deve ser feita a implementação dos testes que assegurem a validade dos demais relacionamentos do Dealer.

## 6 - Regra de negócio importante e como o teste poderia ser implementado

* A Dealer que está sendo criado precisa de um CNPJ válido, então testes que possam averiguar isso são importantes, bem como um CEP válido condizente com o endereço que está sendo informado;

* No momento, é possível criar um novo Dealer sem informar o CEP;

* Alguns outros campos não validam o número máximo/mínimo de caracteres, como Razão Social, domínio e subdomínio;

* Também foi possível criar Dealer com um número de um dígito na cidade e bairro. Talvez uma validação para checar o tipo e a quantidade de caractéres fosse necessária.

## Extras sobre o Dealer.rb

* Dúvida: no model `Dealer` é declarada uma função com o nome `products`, que tem as função de recuperar os produtos daquele dealer que está sendo tratado, e essa função faz isso através do método `Product.by_dealer(dealer)`, uma função também declarada manualmente na classe `Product`. A dúvida é, por que nesse caso não foi declarado um relacionamento entre as classes `Dealer` e `Product`, onde automaticamente seria gerado o método `Dealer.products` que teria o mesmo resultado da função que foi implementada.
